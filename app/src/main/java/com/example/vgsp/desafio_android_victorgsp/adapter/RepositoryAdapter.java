package com.example.vgsp.desafio_android_victorgsp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vgsp.desafio_android_victorgsp.HomeActivity;
import com.example.vgsp.desafio_android_victorgsp.R;
import com.example.vgsp.desafio_android_victorgsp.model.Repository;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by Victor Guedes on 07/01/2018.
 */

public class RepositoryAdapter extends BaseAdapter {
    private final LayoutInflater inflater;
    private final HomeActivity context;
    private List<Repository> repositories;
    int lastIndexSearch = 0;


    public RepositoryAdapter(List<Repository> repositories, HomeActivity context) {
        this.repositories = repositories;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return repositories.size();
    }

    @Override
    public Object getItem(int i) {
        return repositories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (position == repositories.size() - 15 && lastIndexSearch != position){
            context.searchRepositories();
            lastIndexSearch = position;
        }

        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater row = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = row.inflate(R.layout.repository_row, null);

            viewHolder = new ViewHolder();
            viewHolder.mTextViewRepositoryName = convertView.findViewById(R.id.textViewRepositoryName);
            viewHolder.mTextViewDescription = convertView.findViewById(R.id.textViewDescription);
            viewHolder.mTextViewForkCount = convertView.findViewById(R.id.textViewForkCount);
            viewHolder.mTextViewStarCount = convertView.findViewById(R.id.textViewStarCount);
            viewHolder.mTextViewOwnerName = convertView.findViewById(R.id.textViewOwnerName);
            viewHolder.mImageViewUserPhoto = convertView.findViewById(R.id.imageViewUserPhoto);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Repository repository = repositories.get(position);
        viewHolder.mTextViewRepositoryName.setText(repository.getName());
        viewHolder.mTextViewDescription.setText(repository.getDescription());
        viewHolder.mTextViewForkCount.setText(repository.getForksCount());
        viewHolder.mTextViewStarCount.setText(repository.getStargazersCount());
        viewHolder.mTextViewOwnerName.setText(repository.getUser().getLogin());

        viewHolder.mImageViewUserPhoto.setImageBitmap(null);
        if (repository.getUser().getAvatarUrl() != null && !repository.getUser().getAvatarUrl().isEmpty()){
            ImageLoader.getInstance().displayImage(repository.getUser().getAvatarUrl(), viewHolder.mImageViewUserPhoto);
        }

        return convertView;
    }

    static class ViewHolder {
        private TextView mTextViewRepositoryName;
        private TextView mTextViewDescription;
        private TextView mTextViewForkCount;
        private TextView mTextViewStarCount;
        private TextView mTextViewOwnerName;
        private ImageView mImageViewUserPhoto;

    }
}


