package com.example.vgsp.desafio_android_victorgsp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Victor Guedes on 06/01/2018.
 */

public class User implements Serializable {
    private String login;
    @SerializedName("avatar_url")
    private String avatarUrl;


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
