package com.example.vgsp.desafio_android_victorgsp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Victor Guedes on 06/01/2018.
 */

public class PullRequest implements Serializable{


    @SerializedName("html_url")
    private String url;
    private String title;
    private String body;
    @SerializedName("user")
    private User user;
    @SerializedName("created_at")
    private Date createAt;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
